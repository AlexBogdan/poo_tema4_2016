package homework.classes;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import homework.classes.brackets.BracketsFactory;
import homework.classes.operands.OperandsFactory;
import homework.classes.operators.Operator;
import homework.classes.operators.OperatorsFactory;
import homework.classes.operators.binaryOperators.BinaryOperator;
import homework.classes.operators.unaryOperators.UnaryOperator;
import homework.interfaces.IServer;
import homework.interfaces.IToken;
import homework.interfaces.brackets.IBracket;
import homework.interfaces.operands.IOperand;
import homework.interfaces.operators.IOperator;

public final class Server implements IServer {

 private static final double ERROR = 4000;
 private static Server instance = null;
 private String supportedOps = "";
 private List<String> results = new LinkedList<String>();
 private BracketsFactory bracketsF = BracketsFactory.getInstance();
 private OperatorsFactory operatorsF = OperatorsFactory.getInstance();
 @SuppressWarnings("unchecked")
 private OperandsFactory<Double> operandsF =
   (OperandsFactory<Double>) OperandsFactory.getInstance();

 private Server() { }

 public static Server getInstace() {
  if (instance == null) {
   instance = new Server();
  }
  return instance;
 }

 /**
  * Verifica daca nu avem empty line, daca parantezele sunt intr-o ordine
  * corecta si completa si daca toti operatorii ceruti sunt suportati.
  */
 public boolean canPublish(final String[] tokens) {
  if (tokens.length == 0) {
   return false;
  }
  Stack<IBracket> brackets = new Stack<IBracket>();

  for (String s : tokens) {
   IToken cur = new Token(s); // Tokenul curent pe care il verificam
   if (operatorsF.isOperator(cur) && !supportedOps.contains(s)) {
    return false;
   }
   if (bracketsF.isBracket(cur)) {
    IBracket bracket = bracketsF.createBracket(s);
    if (bracketsF.isOpenedBracket(bracket)) {
     brackets.add(bracket);
     continue;
    }
    if (bracketsF.isClosedBracket(bracket)) {
     if (bracketsF.isBracketPair(brackets.peek(), bracket)) {
      brackets.pop();
     } else {
      return false;
     }
    }
   }
  }
  return true;
 }

 public void publish(final String command) {
  String[] tokens = command.split(" ");
  if (!canPublish(tokens)) {
   results.add("IMPOSSIBRU");
   return;
  }
  List<IToken> postfixat = translate(tokens); // Aici cream forma postfixata
  evaluate(postfixat); //Facem evaluarea formei postfixate
 }

 /**
  * Salvam toti operatorii pe care ii vom putea folosi.
  */
 public void subscribe(final String operator) {
  supportedOps += operator;
 }

 public List<String> getResults() {
  return results;
 }

 /**
  * Aici se va transforma linia citita din fisier intr-o forma postfixata care
  * sa poata fi evaluata.
  * @param tokens = Array cu tokenii ce trebuie translatati
  * @return
  */
 private List<IToken> translate(final String[] tokens) {
  // Aici cream forma postfixata
  List<IToken> postfixat = new LinkedList<IToken>();
  //Aici vom tine minte operatorii intalniti
  Stack<IToken> stack = new Stack<IToken>();

  for (String token : tokens) {
   IBracket bracket = bracketsF.createBracket(token);
   IOperator operator = operatorsF.createOperator(token);
   if (bracket != null && bracketsF.isOpenedBracket(bracket)) {
    stack.add(bracketsF.createBracket(token));
    continue;
   } else if (bracket != null && bracketsF.isClosedBracket(bracket)) {
    while (true) {
     IBracket b = bracketsF.createBracket(stack.peek().getSymbol());
     if (bracketsF.isOpenedBracket(b)) {
      break;
     }
     postfixat.add(stack.pop());
    }
    stack.pop(); //Extragem si paranteza inchisa.
    continue;
   } else if (operator != null && operatorsF.isOperator(operator)) {
    if (stack.isEmpty()) {
     stack.add(operator);
     continue;
    }
    while (!stack.isEmpty()
      && !bracketsF.isBracket(bracketsF.createBracket(stack.peek().getSymbol()))
      && operator.getPriority() <= ((Operator) stack.peek()).getPriority()) {
     postfixat.add(stack.pop());
    }
    stack.add(operator);
    continue;
   } else {
    IOperand<Double> operand = operandsF.createOperand(token);
    postfixat.add(operand);
   }
  }
  while (!stack.isEmpty()) {
   postfixat.add(stack.pop());
  }
  return postfixat;
 }

 /**
  * Aici se evalueaza forma poloneza postfixata primita si adauga la results
  * rezultatul.
  * @param postfixat = Lista de Tokeni in forma postfixata
  */
 private void evaluate(final List<IToken> postfixat) {
  Stack<IOperand<Double>> calculator = new Stack<IOperand<Double>>();
  for (IToken t : postfixat) {
   IOperator operator = operatorsF.createOperator(t.getSymbol());
   if (operator != null && operatorsF.isUnaryOperator(operator)) {
    IOperand<Double> a = calculator.pop();
    @SuppressWarnings("unchecked")
    UnaryOperator<Double> op =
      (UnaryOperator<Double>) operatorsF.createOperator(t.getSymbol());
    IOperand<Double> res = op.calculate(a.getSymbolValue());
    calculator.add(res);
   } else if (operator != null && operatorsF.isBinaryOperator(operator)) {
    IOperand<Double> b = calculator.pop();
    IOperand<Double> a = calculator.pop();
    @SuppressWarnings("unchecked")
    BinaryOperator<Double> op =
      (BinaryOperator<Double>) operatorsF.createOperator(t.getSymbol());
    IOperand<Double> res = op.calculate(a.getSymbolValue(), b.getSymbolValue());
    if (res.getSymbolValue().doubleValue() == ERROR) {
     results.add("IMPOSSIBRU");
     return;
    }
    calculator.add(res);
   } else {
    IOperand<Double> res = operandsF.convertToArabNumber(t.getSymbol());
    res.getSymbolValue().doubleValue();
    calculator.add(res);
   }
  }
  IOperand<Double> lastOp = calculator.pop();
  IOperand<Double> finalRes =
    operandsF.convertToRomanNumber(lastOp.getSymbolValue());
  results.add(finalRes.getSymbol());
 }
}
