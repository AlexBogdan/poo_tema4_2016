package homework.classes.operators;

import java.util.HashMap;

import homework.classes.Token;
import homework.interfaces.operators.IOperator;

/**
 * Clasa care va tine minte un operator si va intoarce prioritatea
 * sa intr-un calcul cerut.
 * @author arbiter
 *
 */
public class Operator extends Token implements IOperator {
 private static HashMap<String, Integer> priorities;
 public static final int TREI = 3;
 /**
  * Tinem minte prioritatiile pentru a le intoarce mai usor.
  */
 private void initializePriorities() {
  priorities = new HashMap<String, Integer>();
  priorities.put("+", 0);
  priorities.put("-", 0);
  priorities.put("*", 1);
  priorities.put("/", 1);
  priorities.put("^", 2);
  priorities.put("sqrt", TREI);
  priorities.put("log", TREI);
 }

 /**
  * Initializam un operator (initializam prioritatiile daca nu s-a facut
  * asta anterior.
  * @param symbol = operatorul creat
  */
 public Operator(final String symbol) {
  super(symbol);
  if (priorities == null) {
   initializePriorities();
  }
 }

 public final int getPriority() {
  return priorities.get(super.getSymbol());
 }
}
