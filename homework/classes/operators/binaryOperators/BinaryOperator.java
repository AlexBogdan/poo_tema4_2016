package homework.classes.operators.binaryOperators;

import homework.classes.operands.OperandsFactory;
import homework.classes.operators.Operator;
import homework.interfaces.operands.IOperand;
import homework.interfaces.operators.binaryOperators.IBinaryOperator;

public final class BinaryOperator<T extends Number> extends Operator
   implements IBinaryOperator<T> {
 private static final double ERROR = 4000;

 public BinaryOperator(final String symbol) {
  super(symbol);
 }

 @SuppressWarnings("unchecked")
 public IOperand<T> calculate(final T leftOperand, final T rightOperand) {
  Number result;
  switch (super.getSymbol()) {
   case "+":
    result = leftOperand.doubleValue() + rightOperand.doubleValue();
    break;
   case "-":
    result = leftOperand.doubleValue() - rightOperand.doubleValue();
    break;
   case "*":
    result = leftOperand.doubleValue() * rightOperand.doubleValue();
    break;
   case "/":
    if (rightOperand.doubleValue() == 0) {
     result = ERROR;
     break;
    }
    result = leftOperand.doubleValue() / rightOperand.doubleValue();
    break;
   case "^":
    result = Math.pow(leftOperand.doubleValue(), rightOperand.doubleValue());
    break;
   default:
    result = 0;
  }
  OperandsFactory<T> operandsF =
    (OperandsFactory<T>) OperandsFactory.getInstance();
  IOperand<T> res = operandsF.createOperand("0");
  res.setSymbolValue((T) result);
  return res;
 }

}
