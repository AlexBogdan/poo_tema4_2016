package homework.classes.operators.unaryOperators;

import homework.classes.operands.OperandsFactory;
import homework.classes.operators.Operator;
import homework.interfaces.operands.IOperand;
import homework.interfaces.operators.unaryOperators.IUnaryOperator;

public final class UnaryOperator<T extends Number> extends Operator
   implements IUnaryOperator<T> {

 public UnaryOperator(final String symbol) {
  super(symbol);
 }

 @SuppressWarnings("unchecked")
 @Override
 public IOperand<T> calculate(final T operand) {
  Number result;
  switch (super.getSymbol()) {
   case "sqrt":
    result = Math.sqrt(operand.doubleValue());
    break;
   case "log":
    result = Math.log(operand.doubleValue());
    break;
   default:
    result = 0;
  }
  OperandsFactory<T> operandsF =
           (OperandsFactory<T>) OperandsFactory.getInstance();
  IOperand<T> res = operandsF.createOperand("0");
  res.setSymbolValue((T) result);
  return res;
 }

}
