package homework.classes.operators;

import homework.classes.operators.binaryOperators.BinaryOperator;
import homework.classes.operators.unaryOperators.UnaryOperator;
import homework.interfaces.IToken;
import homework.interfaces.operators.IOperator;
import homework.interfaces.operators.IOperatorsFactory;

/**
 * Clasa care ne va genera operatorii.
 * @author arbiter
 */
public final class OperatorsFactory implements IOperatorsFactory {
 private final String unaryOp = "sqrt log";
 private final String binaryOp = "+-*/^";
 private static OperatorsFactory instance = null;

 private OperatorsFactory() { }

public static OperatorsFactory getInstance() {
 if (instance == null) {
  instance = new OperatorsFactory();
 }
 return instance;
 }

 public IOperator createOperator(final String operator) {
  IOperator op = new Operator(operator);
  if (isOperator(op)) {
   if (isUnaryOperator(op)) {
    return new UnaryOperator<Double>(operator);
   }
   if (isBinaryOperator(op)) {
    return new BinaryOperator<Double>(operator);
   }
  }
  return null;
 }

 public boolean isOperator(final IToken token) {
  if (unaryOp.contains(token.getSymbol())
          || binaryOp.contains(token.getSymbol())) {
   return true;
  }
  return false;
 }

 public boolean isUnaryOperator(final IOperator operator) {
  return unaryOp.contains(operator.getSymbol());
 }

 public boolean isBinaryOperator(final IOperator operator) {
  return binaryOp.contains(operator.getSymbol());
 }

}
