package homework.classes.brackets;

import homework.interfaces.IToken;
import homework.interfaces.brackets.IBracket;
import homework.interfaces.brackets.IBracketsFactory;

/**
 * BracketFactory cu interfata implementata. Multe sanity checks la
 * inceputul fiecarei metode.
 * @author arbiter
 *
 */
public final class BracketsFactory implements IBracketsFactory {
 private static final String OPENED_BRACKETS = "([{";
 private static final String CLOSED_BRACKETS = ")]}";
 private static BracketsFactory instance; // Singleton

 private BracketsFactory() { }

 /**
  * Intoarce unica instanta de Factory.
  * @return
  */
 public static BracketsFactory getInstance() {
  if (instance == null) {
   instance = new BracketsFactory();
  }
  return instance;
 }

 /**
  * Instantiaza un bracket.
  */
 public IBracket createBracket(final String bracket) {
  return new Bracket(bracket);
 }

 /**
  * Verifica daca un token este un bracket.
  */
 public boolean isBracket(final IToken token) {
  if (OPENED_BRACKETS.contains(token.getSymbol())
    || CLOSED_BRACKETS.contains(token.getSymbol())) {
   return true;
  }
  return false;
 }

 /**
  * Verifica daca este o paranteza deschisa.
  */
 public boolean isOpenedBracket(final IBracket bracket) {
  if (OPENED_BRACKETS.contains(bracket.getSymbol())) {
   return true;
  }
  return false;
 }

 /**
  * Verifica daca este o paranteza inchisa.
  */
 public boolean isClosedBracket(final IBracket bracket) {
  if (CLOSED_BRACKETS.contains(bracket.getSymbol())) {
   return true;
  }
  return false;
 }

 /**
  * Daca cele 2 brackets au acelasi index in stringurile definite la inceput
  * atunci ele formeaza o pereche vailda.
  */
 public boolean isBracketPair(final IBracket openBracket,
        final IBracket closeBracket) {
  if (!isOpenedBracket(openBracket) || !isClosedBracket(closeBracket)) {
   return false;
  }

  int i = OPENED_BRACKETS.indexOf(openBracket.getSymbol());
  int j = CLOSED_BRACKETS.indexOf(closeBracket.getSymbol());

  return (i == j);
 }
}
