package homework.classes.brackets;

import homework.classes.Token;
import homework.interfaces.brackets.IBracket;

/**
 * Clasa in care tinem minte o paranteza.
 * @author arbiter
 *
 */
public final class Bracket extends Token implements IBracket {

 Bracket(final String symbol) {
  super(symbol);
 }
}
