package homework.classes;

import homework.interfaces.IToken;

/**
 * Clasa ce stocheaza un anumit simbol.
 * @author arbiter
 *
 */
public class Token implements IToken {

 private String symbol;

 protected Token() { }

 protected Token(final String newSymbol) {
  this.symbol = newSymbol;
 }

 public final String getSymbol() {
  return symbol;
 }

 public final void setSymbol(final String newSymbol) {
  this.symbol = newSymbol;
 }

}
