package homework.classes.operands;

import homework.classes.Token;
import homework.interfaces.operands.IOperand;

/**
 * Clasa in care tinem minte un operand.
 * @author arbiter
 *
 * @param <T>
 */
public final class Operand<T extends Number> extends Token
   implements IOperand<T> {

 private T value;

 public Operand(final String symbol) {
  super(symbol);
 }

 public Operand(final T newValue) {
  this.value = newValue;
 }

 public T getSymbolValue() {
  return value;
 }

 public void setSymbolValue(final T newValue) {
  this.value = newValue;
 }

 /**
  * Just debugging.
  */
 public void display() {
  if (value != null) {
   System.out.println("Operandul este stocat ca : " + value);
   return;
  }
  if (super.getSymbol() != null) {
   System.out.println("Operandul este stocat ca : " + super.getSymbol());
   return;
  }
  System.out.println("Operandul nu este stocat.");
 }

}
