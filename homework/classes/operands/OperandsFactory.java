package homework.classes.operands;

import java.util.HashMap;

import homework.interfaces.operands.IOperand;
import homework.interfaces.operands.IOperandsFactory;

/**
 * Clasa care instantiaza Operands.
 * @author arbiter
 *
 * @param <T>
 */
public final class OperandsFactory<T extends Number>
   implements IOperandsFactory<T> {

private static final int I = 1;
private static final int IV = 4;
private static final int V = 5;
private static final int IX = 9;
private static final int X = 10;
private static final int XL = 40;
private static final int L = 50;
private static final int XC = 90;
private static final int C = 100;
private static final int CD = 400;
private static final int D = 500;
private static final int CM = 900;
private static final int M = 1000;

 /**
  * Tinem minte instanta de OperandsFactory pe care sa o intoarcem.
  * @author arbiter
  *
  */
 @SuppressWarnings("rawtypes")
 private static OperandsFactory instance = null;
 private static HashMap<Integer, String> arabic;
 private static HashMap<String, Integer> roman;

 /**
  * Setam HashMap-ul pentru numere arabe.
  */
 private void initializeArabicHash() {
  arabic = new HashMap<Integer, String>();
  arabic.put(I, "I");
  arabic.put(IV, "IV");
  arabic.put(V, "V");
  arabic.put(IX, "IX");
  arabic.put(X, "X");
  arabic.put(XL, "XL");
  arabic.put(L, "L");
  arabic.put(XC, "XC");
  arabic.put(C, "C");
  arabic.put(CD, "CD");
  arabic.put(D, "D");
  arabic.put(CM, "CM");
  arabic.put(M, "M");
 }

 /**
  * Setam HashMap-ul pentru numere romane.
  */
 private void initializeRomanHash() {
  roman = new HashMap<String, Integer>();
  roman.put("I", I);
  roman.put("IV", IV);
  roman.put("V", V);
  roman.put("IX", IX);
  roman.put("X", X);
  roman.put("XL", XL);
  roman.put("L", L);
  roman.put("XC", XC);
  roman.put("C", C);
  roman.put("CD", CD);
  roman.put("D", D);
  roman.put("CM", CM);
  roman.put("M", M);
 }

 private OperandsFactory() {
  initializeArabicHash();
  initializeRomanHash();
 }

 /**
  * Intoarcem mereu aceeasi instanta de Factory.
  * @return
  */
 @SuppressWarnings({ "unchecked", "rawtypes" })
 public static OperandsFactory<? extends Number> getInstance() {
  if (instance == null) {
   instance = new OperandsFactory();
  }
  return instance;
 }

 /**
  * Creeaza un operand nou.
  */
 public IOperand<T> createOperand(final String operand) {
  return new Operand<T>(operand);
 }

 /**
  * Returneaza o instanta noua de Operand care tine minte un numar roman.
  */
 public Operand<T> convertToRomanNumber(final T arabNumber) {
  int number = arabNumber.intValue(); // Facem o copie a numarului
  String romanNumber = "";
  if (arabNumber.doubleValue() < 0) {
   romanNumber = "- ";
   number = -number;
   if (arabNumber.intValue() != arabNumber.doubleValue()) {
    number += 1;
   }
  }
  int[] arabicValues =
   {M, CM, D, CD, C, XC, L, XL, X, IX, V, IV, I};
  for (int i = 0; i < arabicValues.length; i++) {
   while (number - arabicValues[i] >= 0) {
    romanNumber += arabic.get(arabicValues[i]);
    number -= arabicValues[i];
   }
  }
  return new Operand<T>(romanNumber);
 }

 /**
  * Returneaza o instanta noua de Operand care tine minte un numar arab.
  */
 @SuppressWarnings("unchecked")
 public Operand<T> convertToArabNumber(final String romanNumber) {
  String romanNr = romanNumber;
  Double arabNumber = (double) 0;
  String current = "";
  while (!romanNr.isEmpty()) {
   current = "";
   if (romanNr.length() >= 2) {
    current = romanNr.substring(0, 2);
   }
   if (roman.containsKey(current)) {
    arabNumber += roman.get(current);
    romanNr = romanNr.substring(current.length());
    continue;
   }

   if (romanNr.length() >= 1) {
    current = romanNr.substring(0, 1);
   }
   if (roman.containsKey(current)) {
    arabNumber += roman.get(current);
    romanNr = romanNr.substring(current.length());
    continue;
   }
  }
  if (romanNr.contains("-")) {
   arabNumber  = -arabNumber;
  }
  return new Operand<T>(((T) arabNumber));
 }

}
